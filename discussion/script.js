// Mathematical Operators

/*
	+ Sum
	- Difference
*/

function mod(){
	// return 9+2;
	// return 9-2
	// return 9*2;
	// return 9/2;
	// return 9%2;
	let x = 10;
	// return x += 2;
	// return x -= 2;
	// return x *= 2;
	// return x /= 2;
	// return x % 2;
};

console.log(mod());

// Assignment Operator (=) is used for assigning values to variables

let x = 1;
let sum = 1;
// sum = sum + 1
x -=1;
sum += 1;

console.log(sum);
console.log(x);

// Increment & Decrement

// Pre-increment - add 1 before assigning the value to the variable

/*let z = 4;
let z = (1) + 4;
let increment = ++z;

console.log(`Result of pre-increment: ${increment}`);
console.log(`Result of pre-increment: ${z}`);*/

// Post-increment - add 1 AFTER assigining the value to the variable
/*let z = 1;
let z = 4 + (1);
let increment = z++;

console.log(`Result of post-increment: ${increment}`);
console.log(`Result of post-increment: ${z}`);*/

// Decrement

// Pre-decrement - deducting 1 BEFORE the assigning of values (basing from the most recent value of z)
/*let z = 1;
let z = 4 + (1);
let decrement = --z;

console.log(`Result of pre-decrement: ${decrement}`);
console.log(`Result of pre-decrement: ${z}`);*/

// Post-decrement - subtracting 1 AFTER assigning of values

/*let z = 1;
let z = 4 + (1);
let decrement = z--;

console.log(`Result of post-decrement: ${decrement}`);
console.log(`Result of post-decrement: ${z}`);*/




// Comparison Operators
// Equality Operator (==) - compares if the two values are equal

let juan = "juan";

console.log(1 == 1);
console.log(0 == false);
console.log(juan == "juan");

// Strict Equality (===) - 

console.log(1 === 1);
console.log(1 === true);
// the javascript is not anymore concerned with other relative functions of the data, it only compares the two vlaues as they are;


// Inequality Operator (!=)
console.log(1 != 1);
console.log(juan != "Juan");


// Logical Operators
/*
	AND Operator (&&) - returns true if all operands are true;
		true + true = true
		true + false = false
		false + true = false
		false + false = false
*/

// And Operator
let isLegalAge = true;
let isRegistered = true;

let allRequirementMet = isLegalAge && isRegistered;
console.log(`Result of logical AND operator: ${allRequirementMet}`);

/*
	OR Operator (||) - returns true if all operands are true;
		true + true = true
		true + false = true
		false + true = true
		false + false = false
*/

// Or Operator (||)
allRequirementMet = isLegalAge || isRegistered;
console.log(`Result of logical OR operator: ${allRequirementMet}`);

// Selection Control Structures

/*
	IF Statements - executes a command if a specified condition is true
		SYNTAX:
			if (condition) {
				statements/command
			}
*/

let num = -1;
if (num<0){
	console.log("Hello");
};

let value = 30;
if (value > 10 || value < 40){
	console.log(`Welcome to Zuitt`);
};



/*
	If-Else Statement - executes a command if the first condition returns false
		if(condition){
			statement if true
		} else {
			statement if false
		}
*/

num = 5
if (num > 10) {
	console.log(`Number is greater than 10`);
} else {
	console.log(`Number is less than 10`);
};

/*
	prompt - dialog box that has input fields
	alert - dialog box that has no input fields; used for warnings, announcement etc.
	parseInt - converts number in string data type into numerical data type. it only recognizes numbers and ignores the alphabets (it returns NaN - Not a Number)
*/

/*num = parseInt(prompt("Pease input a number."));

if (num > 59){
	alert("Senior Age")
	console.log(num)
} else {
	alert("Invalid Age")
	console.log(num)
};*/

//if-elseif-else statement
/*
	SYNTAX:
		if(condition){
			statement
		}else if(condition){
			statement
		}else if(condition){
			statement
		}else{
			statement
		};
*/

/*
	1. Quezon
	2. Valenzuela
	3. Pasig
	4. Taguig
*/

/*let city = parseInt(prompt("Enter a Number"));
if (city === 1) {
	alert("Welcome to Quezon City")
} else if (city === 2){
	alert("Welcome to Valenzuela City")
} else if ( city === 3 ){
	alert("Welcome to Pasig City")
} else if ( city === 4 ){
	alert("Welcome to Taguig City")
} else {
	alert("Invalid Number")
};*/


let message = ""

function determineTyphoonIntensity(windspeed){
	if(windspeed < 30){
		return `Not a typhoon yet.`
	} else if (windspeed <= 61) {
		return `Tropical Depression detected.`
	} else if (windspeed >= 62 && windspeed <= 88) {
		return `Tropical Storm detected.`
	} else if (windspeed >= 89 && windspeed <= 117) {
		return `Sever Tropical Storm detected`
	} else {
		return `Typhoon detected`
	};
}

message = determineTyphoonIntensity(63);
console.log(message);

/*
	Ternary operator - shorthanded if-else statement
		SYNTAX:
			(condition) ? ifTrue : ifFalse;
*/

/*let ternaryResult = (1 < 18) ? true : false;
console.log(`Result of ternary operator: ${ternaryResult}`);

let name;
function isOfLegalAge(){
	name = "John";
	return "You are of the age Limit"
};

function isUnderAge(){
	name = "Jane";
	return "You are under the Legal Age"
};

let age = parseInt(prompt("What is your age?"));
let LegalAge = (age >= 18) ? isOfLegalAge() : isUnderAge();
alert(`Result of ternary operator in function: ${LegalAge} ${name}`);*/



// Switch Statement

/*
	SYNTAX
		switch (expression/parameter){
			case value1:
				statement/s;
				break;
			case value2:
				statement/s;
				break;
			case value3:
				statement/s;
				break;
			case value4:
				statement/s;
				break;
			case value5:
				statement/s;
				break;
			default:
				statements;
		}
*/

/*let day = prompt("What day is it today?").toLowerCase(); switch(day){
	case "sunday":
		alert("The color of the day is red");
		break;
	case "monday":
		alert("The color of the day is orange");
		break;
	case "tuesday":
		alert("The color of the day is yellow");
		break;
	case "wednesday":
		alert("The color of the day is green");
		break;
	case "thursday":
		alert("The color of the day is blue");
		break;
	case "friday":
		alert("The color of the day is violet");
		break;
	case "saturday":
		alert("The color of the day is indigo");
		break;
	default:
		alert("Please input a valid day");
};*/


// Try-Catch-Finally Statement

function showIntentsityAlert(windspeed){
	try{
		alert(determineTyphoonIntensity(windspeed));
	}
	catch(error){
		console.log(typeof error);
		console.warn(error.message);
	}
	finally{
		alert("Intensity updates will show new alert")
	}
};
showIntentsityAlert(30);
// showIntensityAlert(1e); (WITH ERROR)
